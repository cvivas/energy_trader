Java exercise involving math formulas and excel file reading / writing. 
=======================================================================

By: *Carlos Vivas Abilahoud*

Solution to the exercise presented in Exercise.pdf.

The complexity of the main algorithm is linear O(category x days_in_year x hours).

I use the Apache POI library to read the "input.xls" file and write the output on the "output.xls" file

The function used to calculate the final profiles is a simplification of the one in the pdf file. By applying mathematical procedures we can obtain a simplified version with the same results.

For a detailed version of the mathematical procedures used to simplify, please consult the attached document explaining it called "Formula Simplification.pdf"