/*
 * 
 * Carlos Vivas
 * carlos.vivas.a@gmail.com
 * 
 * */

package energy_traders;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class FinalProfiles {

	/*
	 * Note: I decided to use doubles for the sake of simplicity and speed.
	 * If we wanted really exact values, we should use BigDecimal instead.
	 * However, Excel values are stored as double via the Apache POI.
	 * */
	private double[][] day_values = new double[8760][4]; // array with the values of the
													// initial profiles
	private double[][] accumulated_day_values = new double[8760][4]; // accumulated
																// values of the
																// initial
																// profiles for
																// each day
	private double full_day = 0; // sum of the coefficients of the initial
									// profile of the customer category for 24
									// hours
	private double accumulated_year = 0; // accumulated value of full_day
	
	private double[][] result_day = new double[8760][4];

	/*
	 * function that calculates the final profiles. the complexity is linear,
	 * more exactly : O(client_category * days * hour)*3 
	 * (one loop to read, one loop to treat data, one loop to write) 
	 * 
	 * 	 * So complexity is:  O(client_category * days * hour) 
	 */
	public void calculate_final_profiles(int total_days) {
		double up = 0;
		double down = 0;
		double coef = 0;
		double multiplier = 0;
		
		get_all_client_values_year(total_days);

		// for each category a, b, c, d
		for (int client_category = 0; client_category < 4; client_category++) {
			coef = get_gamma(client_category); // get category from excel file
			coef = 1 - (coef * 0.13);

			this.accumulated_year = 0;
			// iteration for each of the days of the year
			
			int accumulated_day_index = -1;
			int day_values_index = 0;
			for (int day = 0; day < total_days; day++) {
				
				accumulated_day_index +=24; //increase day in accumulated day index points to the last hour of the accumulated_day_values of the day "day"
				// full day and accumulated day values updated
				this.full_day = this.accumulated_day_values[accumulated_day_index][client_category];
				this.accumulated_year += this.full_day;

				multiplier = coef * full_day; // multiplier for that day
				
				//for each hour of the day
				for (int i = 0; i < 24; i++) {
					up = this.day_values[i+day_values_index][client_category] * multiplier; // upper part (numerator) of the division
					down = this.accumulated_day_values[i+day_values_index][client_category] * this.accumulated_year;
					// lower part (denominator) of the division
					result_day[i+day_values_index][client_category] = up / down; // result of the value of the final
										// profile for the user category for the
										// hour, day and month of a given year
					
				}
				day_values_index+=24;
			}
		}
		this.write_year_values();
	}

	private void get_all_client_values_year(int total_days) {
		/*
		 * get all the values 
		 * puts values in day_values array and 
		 * accumulated values in accumulated_day_values array
		 * Linear complexity
		 * */

		try {
		     
		    FileInputStream file = new FileInputStream(new File("input.xls"));
		     
		    //Get the workbook instance for XLS file 
		    HSSFWorkbook workbook = new HSSFWorkbook(file);
		 
		    //Get first sheet from the workbook
		    HSSFSheet sheet = workbook.getSheetAt(0);
		    Cell cell = null;
		    for(int category = 0; category < 4; category ++){
			    int rowNumber = 2 ;
			    int colNumber = 3 + category;
			    double value = 0;
				// fill the day_values for the year for the client category
			    int limit = total_days * 24;
				for (int index = 0; index < limit; index++) {
					cell = sheet.getRow(rowNumber + index ).getCell(colNumber);
					value = cell.getNumericCellValue();
					this.day_values[index][category] = value;
					//fill accumulated day values, need to restart accumulated sum each 24 elements
					if (index % 24 == 0){
						this.accumulated_day_values[index][category] = this.day_values[index][category];
					}
					else {
						this.accumulated_day_values[index][category] = this.day_values[index][category] + this.accumulated_day_values[index -1][category];
					}
				}
		    
		    }
		    file.close();
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}


	private void write_year_values() {
		/*
		 * write all the values in the results_day array to the excel file output.xls
		 * Linear complexity
		 * */
		
		try {
		     
		    FileInputStream file = new FileInputStream(new File("output.xls"));
		     
		  //Get the workbook instance for XLS file 
		    HSSFWorkbook workbook = new HSSFWorkbook(file);
		 
		    //Get first sheet from the workbook
		    HSSFSheet sheet = workbook.getSheetAt(0);
		    
		    
		    int rowNumberBase = 1  ;
		    for(int category = 0; category <4; category++){
		    	 for (int i = 0; i < this.result_day.length; i++) {
				    	int rowNumber = rowNumberBase+i;
						 /*
						  *  WRITE IN EXCEL FILE
						  */
						//create row and cell if needed
						Row row = sheet.getRow(rowNumber);
					    if(row == null) sheet.createRow(rowNumber);
					    row = sheet.getRow(rowNumber);
					    Cell cell = row.getCell(category);
					    if(cell == null){
					    	//write in cell
					    	cell = row.createCell(category);
					    	cell.setCellType(Cell.CELL_TYPE_NUMERIC);
					    	cell.setCellValue(this.result_day[i][category]);
					    }
					    else{
					    	System.out.println("error writing in file, cell already exists");
					    }
		    	 }
		    }
		    
		    file.close();
		    FileOutputStream fileOut = new FileOutputStream("output.xls");
		    workbook.write(fileOut);
		    fileOut.close();
		    
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}



	public double get_gamma(int client_category) {
		/*
		 * get gamma value for client category = client_category
		 * */
		double gamma = 0;
		try {
		     
		    FileInputStream file = new FileInputStream(new File("input.xls"));
		     
		    //Get the workbook instance for XLS file 
		    HSSFWorkbook workbook = new HSSFWorkbook(file);
		 
		    //Get second sheet from the workbook
		    HSSFSheet sheet = workbook.getSheetAt(1);
		    Cell cell = null;
		    
		    //get gamma of desired client category
		    int rowNumber = 3;
		    int colNumber = 1 + client_category;
		    cell = sheet.getRow(rowNumber).getCell(colNumber);
		    
		    gamma = cell.getNumericCellValue();
		    file.close();
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return gamma;	
		 
	}

	public void create_output_file() {
		/* create a blank excel file. 
		 * If there is already an excel file with the name output.xls
		 *  the function will delete the file */
		File f = new File("output.xls");
		if(f.exists()){
			//if there is an excel file, delete it
			f.delete();
			
		}
		
		//create blank excel file
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("calculated_output");
		HSSFRow row = sheet.createRow(0);
		HSSFCell cell = row.createCell(0);
		cell.setCellValue(new HSSFRichTextString("Output Category A"));
		cell = row.createCell(1);
		cell.setCellValue(new HSSFRichTextString("Output Category B"));
		cell = row.createCell(2);
		cell.setCellValue(new HSSFRichTextString("Output Category C"));
		cell = row.createCell(3);
		cell.setCellValue(new HSSFRichTextString("Output Category D"));
		 
		try {
			
		    FileOutputStream out = new FileOutputStream(new File("output.xls"));
		    workbook.write(out);
		    out.close();
		    System.out.println("Excel file created successfully..");
		    
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
			
		
	}

}
