/*
 * 
 * Carlos Vivas
 * carlos.vivas.a@gmail.com
 * 
 * */


package energy_traders;

public class Main {

	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		
		FinalProfiles f = new FinalProfiles();
		// create output file
		f.create_output_file();
		
		//calculate final profiles
		f.calculate_final_profiles(365);
		
		//show time
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		
		long ns = totalTime % 1000;
		long micros = (totalTime/1000)%1000;
		long milis = (totalTime/1000000)%1000;
		long second =(totalTime/1000000000)%60;
		long minute = (totalTime/1000000000) / 60;
		
		System.out.println("took: "+ minute + " minutes "+ second + " seconds " + milis + " milis " + micros + " micros " +  ns + " ns");

	}

}
	